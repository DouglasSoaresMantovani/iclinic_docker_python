Execucao do container
 
A forma abaixo descreve a forma a ser executado o container docker
A chamada deve ser configurada no jenkins com acesso remoto ao servidor docker 
O container sera implementado pelo jenkins com o plugin maskerade password.


docker run -dit -p 80:80 -v /mnt/app:/mnt/app --env ICLINIC_PASS=${token} --name=iclinic  python:devops "/bin/bash"
docker exec iclinic /usr/local/bin/python /mnt/app/app.py &
