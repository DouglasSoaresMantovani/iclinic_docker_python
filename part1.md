Para alteracao do token ,acessar a url do job jenkins:

http://ec2-54-158-5-31.compute-1.amazonaws.com:8080/job/Iclinic_deploy_container/configure

Na aba ambiente de build, No campo Name/Password Pairs, trocar o valor do token na variavel de nome token.
O valor do token e passado ao container como variavel durante o deploy.

Pelo console portainer, destruir o container caso exista.
http://ec2-54-158-5-31.compute-1.amazonaws.com:9000/#/dashboard

Executar o job para o redeploy do container novo com o ultimo token definido.
http://ec2-54-158-5-31.compute-1.amazonaws.com:8080/job/Iclinic_deploy_container.

